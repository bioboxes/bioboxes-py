import pytest, helper, tempfile
import biobox.image.execute as exe

from biobox.exception import BioboxInputVolumeNotFound

IMAGE = 'bioboxes/crash-test-biobox@sha256:fdfdda8192dd919e6cac37366784ec8cfbf52c6fec53fe942a7f1940bd7642e8'

def config(path):
    return [{"fastq": [
                {"id": 0, "type": "paired", "value": path}]}]


def test_create_container_with_cgroup_data():
    # Currently {'cpuset' : "0"} is too hard to test because I don't know
    # how to get the CPU IDs for the machine being used for testing
    args = [{'cpu_shares' : 1}, {'mem_limit' : 10000000}]
    out_dir = tempfile.mkdtemp()
    for arg in args:
        cnt = exe.create_container(
                IMAGE,
                config(helper.short_read_fastq()),
                {"output" : out_dir},
                "short_read_assembler",
                "0.9.0",
                arg)
        helper.clean_up_container(cnt['Id'])


def test_create_container_with_missing_input_volume():
    out_dir = tempfile.mkdtemp()
    with pytest.raises(BioboxInputVolumeNotFound) as excp:
        exe.create_container(
                IMAGE,
                config('unknown_path'),
                {"output" : out_dir},
                "short_read_assembler",
                "0.9.0")
